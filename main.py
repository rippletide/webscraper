# Web Scraper to compile list of US drug companies from https://www.drugs.com/pharmaceutical-companies.html
# Shane Cooper - metalrack@protonmail.com
from bs4 import BeautifulSoup
import pandas as pd
import requests

print("\nNOTE: This operation may take several minutes to complete.")
print("Scraping data from https://www.drugs.com/pharmaceutical-companies.html")
print("Working....\n")


############################################################################
# Script 1 - scrape companies and info links from drugs.com and save to file
############################################################################
def get_companies(source):
    info = requests.get(source).text
    soup = BeautifulSoup(info, 'lxml')
    table_list = []

    ddc = soup.find('div', class_='ddc-grid')

    name_lst = ddc.find_all('a')

    for n in name_lst:
        co = n.text
        lnk = ('https://www.drugs.com' + n.get('href'))
        co_lst = {
            'NAME': co,
            'INFO LINK': lnk
        }

        table_list.append(co_lst)

    return table_list


# Call script 1 function
data = get_companies('https://www.drugs.com/pharmaceutical-companies.html')

# Create dataframe
df = pd.DataFrame(data)
df = df.set_index('NAME')
df.to_csv('pharma_table.csv')
df.to_excel('pharma_table.xlsx')

##################################################################################
# Script 2 - opens company info link and compiles company info into existing tables
##################################################################################

address = []
phone = []
website = []
company_info = []


# Function opens link in INFO LINK column of pharma_table.xlsx and scrapes data from
# the company information page. It then populates addresses, phone numbers and web address
# in columns with associated column names.
def get_co_info():

    # read Excel file
    dfr = pd.read_excel(r'pharma_table.xlsx')

    d_lst = []
    for index in range(len(dfr)):
        link = requests.get(dfr.iloc[index, 1]).text
        soup = BeautifulSoup(link, 'lxml')

        try:
            info = soup.find('div', class_='ddc-manufacturer-details')

            datas = info.find('p')

            for d in datas:
                d = d.text
                d_lst.append(d)

            company_info.append([*d_lst])
            d_lst.clear()

        except AttributeError:
            info = soup.find('div', class_='contentBox')

            d = info.p.text

            company_info.append(d)

    # Remove company name
    for item in company_info:
        try:
            del item[0]
        except TypeError:
            pass

    # Change string items to list and add placeholders
    for item in company_info:
        if type(item) == str:
            n = company_info.index(item)
            n_item = [item, ' Phone: N/A', 'Website: N/A']
            company_info.pop(n)
            company_info.insert(n, n_item)

    # Adding website N/A placeholder to entries without web address
    for item in company_info:

        # Initializing substring
        subs = 'Website'

        # using list comprehension
        # to get string with substring
        res = [i for i in item if subs in i]

        # Finding results
        if "Website" in (str(res)):
            pass
        else:
            item.append('Website: N/A')

    # Adding phone N/A placeholder to entries without web address
    for item in company_info:

        # Initializing substring
        subs = 'Phone'

        # using list comprehension
        # to get string with substring
        res = [i for i in item if subs in i]

        # Finding results
        if "Phone" in (str(res)):
            pass
        else:
            item.append('Phone: N/A')

    # Remove empty strings
    for item in company_info:
        while '' in item:
            item.remove('')
        while ' ' in item:
            item.remove(' ')

    # scrape phone number
    for item in company_info:
        for i in item:
            if "Phone" in i:
                phone.append(item[item.index(i)])
                item.pop(item.index(i))

    # scrape web address
    for item in company_info:
        for i in item:
            if "Website" in i:
                website.append(item[item.index(i)])
                item.pop(item.index(i))

    # delete careers
    for item in company_info:
        for i in item:
            if "Careers" in i:
                item.pop(item.index(i))

    # Delete patient assistance
    for item in company_info:
        for i in item:
            if "Patient" in i:
                item.pop(item.index(i))

    # Remove line breaks
    for items in company_info:
        a = ' '.join(items)
        a = a.replace('\r', '')
        a = a.replace('\n', '')
        address.append(a)

    dfr['ADDRESS'] = address
    dfr['PHONE'] = phone
    dfr['WEBSITE'] = website

    return dfr


# Call Script 2 function
data = get_co_info()

df = pd.DataFrame(data)
df = df.set_index('NAME')
df.to_csv('pharma_table.csv')
df.to_excel('pharma_table.xlsx')

print("Operation completed. Data saved to file.")
